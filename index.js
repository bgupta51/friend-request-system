const express = require("express");
const bodyParser = require('body-parser')
const app = express();
const mongoose = require("mongoose");
const dotenv = require("dotenv");
const helmet = require("helmet"); //make secure http headers
const morgan = require("morgan"); //middleware to log requests
const userRoute = require("./routes/users");
const authRoute = require("./routes/auth");
const friendRoute = require("./routes/friends");
dotenv.config(); //zero dependency module that loads env variables

main().catch(err => console.log(err));

async function main() {
    await mongoose.connect(process.env.MONGO_URL, { useNewUrlParser: true }, () => {
        console.log("Connected to MongoDB");
    });
}

//middlewares
app.use(bodyParser.json());
app.use(express.json());
app.use(helmet());
app.use(morgan("common"));

app.use("/api/users", userRoute);
app.use("/api/auth", authRoute);
app.use("/api/friends", friendRoute);

app.listen(5050, () => {
    console.log("Backend Server is running!")
})