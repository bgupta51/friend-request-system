const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema({
        username: {
            type: String,
            require: true,
            min: 3,
            max: 20,
            unique: true,
        },

        email: {
            type: String,
            required: true,
            max: 50,
            unique: true,
        },

        password: {
            type: String,
            required: true,
            min: 6,
        },
        isAdmin: {
            type: Boolean,
            default: false,
        },
        age: {
            type: Number,
            default: null
        },

        city: {
            type: String,
            default: ''
        },
        //user has sent
        sentRequest: [{
            username: {
                type: String,
                default: ''
            }
        }],
        //user has received
        request: [{
            userId: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
            },
            username: {
                type: String,
                default: ''
            }
        }],
        //user's friend list
        friendsList: [{
            friendId: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
            },
            friendName: {
                type: String,
                default: ''
            }
        }],

    }, { timestamps: true }


);

module.exports = mongoose.model("User", UserSchema);