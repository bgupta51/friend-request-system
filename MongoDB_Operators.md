                                                    MongoDB Operators


Query and Projection Operators
    
1) Comparison Operators

$eq: Matches values that are equal to a specified value. 
    
Example:  db.inventory.find( { qty: { $eq: 20 } } )

$gt: Matches values that are greater than a specified value.
    
    Example: db.inventory.find( { quantity: { $gt: 20 } } )

$gte: Matches values that are greater than or equal to a specified value.

$in: Matches any of the values specified in an array.
    
    Example: db.inventory.find( { quantity: { $in: [ 5, 15 ] } }, { _id: 0 } )

$lt: Matches values that are less than a specified value.

$lte: Matches values that are less than or equal to a specified value.

$ne: Matches all values that are not equal to a specified value

$nin: Matches none of the values specified in an array.
    
Example: db.inventory.find( { quantity: { $nin: [ 5, 15 ] } }, { _id: 0 } )


2)Logical 

$and: $and performs a logical AND operation on an array of one or more expressions.

Example: db.example.find( {
   			$and: [
      				{ x: { $ne: 0 } },

      				{ $expr: { $eq: [ { $divide: [ 1, "$x" ] }, 3 ] } }

   				  ]
} )

$not: $not performs a logical NOT operation on the specified <operator-expression> and selects the documents that do not match the <operator-expression>. This includes documents that do not contain the field.

Example: db.inventory.find( { price: { $not: { $gt: 1.99 } } } )

This query will select all documents in the inventory collection where:

->the price field value is less than or equal to 1.99 or
->the price field does not exist
{ $not: { $gt: 1.99 } } is different from the $lte operator. { $lte: 1.99 } returns only the documents where price field exists and its value is less than or equal to 1.99.

$nor: $nor performs a logical NOR operation on an array of one or more query expression and selects the documents that fail all the query expressions in the array. The $nor has the following syntax

Example:

db.inventory.find( { $nor: [ { price: 1.99 }, { sale: true } ]  } )

Or: The $or operator performs a logical OR operation on an array of two or more <expressions> and selects the documents that satisfy at least one of the <expressions>. The $or has the following syntax:
Example:

db.inventory.find( { $or: [ { quantity: { $lt: 20 } }, { price: 10 } ] } )

3)Element

$exists

Syntax: { field: { $exists: <boolean> } }

When <boolean> is true, $exists matches the documents that contain the field, including 	documents where the field value is null. If <boolean> is false, the query returns only the documents 	that do not contain the field. [1]

MongoDB $exists does not correspond to SQL operator exists. For SQL exists, refer to 	the $in operator.

Example:

db.inventory.find( { qty: { $exists: true, $nin: [ 5, 15 ] } } )

This query will select all documents in the inventory collection where the qty field exists and its 	value does not equal 5 or 15.
