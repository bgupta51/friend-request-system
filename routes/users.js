const router = require("express").Router();
const User = require("../models/User");
const bcrypt = require("bcrypt");
const mongoose = require("mongoose");

//update user
router.put('/useId/:id', async(req, res) => {
    const userId = req.params.id;
    try {

        const foundUser = await User.findOne({
            _id: mongoose.Types.ObjectId(userId),
        });


        const passwordsMatch = await bcrypt.compare(req.body.password, foundUser.password);
        if (!passwordsMatch) {
            const error = new Error('Not authenticated');
            error.code = 401;
            res.status(401).send(error);
        } else {
            const user = User.findOneAndUpdate({ _id: mongoose.Types.ObjectId(userId), }, { $set: req.body });
        }
        res.status(200).json("Account has been updated");
    } catch (err) {
        return res.status(403).json("You can update only your account!");
    }

});

router.put("/:id", async(req, res) => {
    if (req.body.userId === req.params.id || req.body.isAdmin) {
        if (req.body.password) {
            try {
                const salt = await bcrypt.genSalt(10);
                req.body.password = await bcrypt.hash(req.body.password, salt);
            } catch (err) {
                return res.status(500).json(err);
            }
        }
        try {
            const user = await User.findByIdAndUpdate(req.params.id, {
                $set: req.body,
            });
            res.status(200).json("Account has been updated");
        } catch (err) {
            return res.status(500).json(err);
        }
    } else {
        return res.status(403).json("You can update only your account!");
    }
});



//delete user
router.delete("/:id", async(req, res) => {
    if (req.body.userId === req.params.id || req.body.isAdmin) {
        try {
            await User.findByIdAndDelete(req.params.id);
            res.status(200).json("Account has been deleted");
        } catch (err) {
            return res.status(500).json(err);
        }
    } else {
        return res.status(403).json("You can delete only your account!");
    }
});


module.exports = router;