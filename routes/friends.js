const router = require("express").Router();
const User = require("../models/User");
const bcrypt = require("bcrypt");
const mongoose = require("mongoose");

router.put('/:id', async(req, res) => {
    try {
        const myid = req.body.userId;
        const mydetails = await User.findById(myid);
        mydetails.sentRequest.push(req.params.id);
        const id = req.params.id;
        mydetails.save()
        User.updateOne({ '_id': id }, { $push: { request: { userId: myid, username: mydetails.username } } });
        res.status(200).json("Friend request sent");
    } catch (err) {
        return res.status(500).json(err);
    }
});


//accepting the friend sentRequest 
// for this we have to remove the sent request from user's id and update both friendlist
router.put('/accept/:friendid', async(req, res) => {
    try {
        const frienId = req.params.friendid;
        const friendDetails = await User.findById(frienId);
        const myId = req.body.userId;
        // this function is updated for the receiver of the friend request when it is accepted
        User.updateOne({ '_id': myId, 'friendsList.friendId': { $ne: frienId } }, {
            $push: { friendsList: { friendId: frienId, friendName: frienId.userName } },
            $pull: { request: { userId: frienId, username: frienId.userName } }
        });
        res.status(200).json("Friend request accepted");
    } catch (err) {
        console.log("Error");
    }


});





router.delete('/user/:id', async(req, res) => {
    try {
        const myid = req.params.id
        await User.findByIdAndDelete(myid)
        await User.updateMany({}, { $pull: { sentRequest: myid, friends: myid, pending: myid } })
        res.status(200).json({ message: "deleted" })
    } catch (error) {
        console.log(error)
    }
})

module.exports = router;